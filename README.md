# go0

Go0 is a [self] educational toy Go compiler for a minimalistic subset of
Go. It demonstrates how to use the modernc.org/libqbe compiler back end.

# Installation

    $ go install modernc.org/go0@latest

# Documentation

See [pkg.go.dev/modernc.org/go0](https://pkg.go.dev/modernc.org/go0)
