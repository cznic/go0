// Copyright 2023 The Go0 Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"os"
	"strings"
	"testing"
)

func TestMain(m *testing.M) {
	rc := m.Run()
	os.Exit(rc)
}

func Test(t *testing.T) {
	dir := t.TempDir()
	var b strings.Builder
	main0(dir, &b)
	const want = "\n267914296\n"
	if !strings.Contains(b.String(), want) {
		t.Fatalf("expected output not found: %q", want)
	}
}
