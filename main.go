// Copyright 2023 The Go0 Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"bytes"
	"fmt"
	"go/token"
	"go/types"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"

	"golang.org/x/tools/go/packages"
	"golang.org/x/tools/go/ssa"
	"golang.org/x/tools/go/ssa/ssautil"
	"modernc.org/cc/v4"
	"modernc.org/libqbe"
)

const src = `package main

func fib(a int) int {
	if a < 2 {
		return a
	}
	return fib(a-1) + fib(a-2)
}

func main() {
	println(fib(42))
}
`

var (
	goos   = runtime.GOOS
	goarch = runtime.GOARCH
)

func main() {
	dir, err := os.MkdirTemp("", "go0-")
	if err != nil {
		panic(err)
	}

	main0(dir, os.Stdout)
}

func main0(dir string, stdout io.Writer) {
	fn := filepath.Join(dir, "fib.go")
	if err := os.WriteFile(fn, []byte(src), 0660); err != nil {
		panic(err)
	}

	cmd := exec.Command("go", "mod", "init", "example.com/fib")
	cmd.Dir = dir
	out, err := cmd.CombinedOutput()
	if err != nil {
		panic(fmt.Errorf("\nout=%s FAIL err=%v", out, err))
	}

	fmt.Fprintf(stdout, "---- Go\n%s\n", src)
	cfg := packages.Config{Dir: dir, Mode: packages.LoadAllSyntax}
	initial, err := packages.Load(&cfg, "example.com/fib")
	if err != nil {
		panic(err)
	}

	if packages.PrintErrors(initial) != 0 {
		panic("FAIL")
	}

	fmt.Fprintf(stdout, "---- SSA\n")
	prog, _ := ssautil.AllPackages(initial, ssa.PrintFunctions|ssa.InstantiateGenerics)
	prog.Build()
	var qbe, asm bytes.Buffer
	w := &writer{&qbe}
	pkg := prog.AllPackages()[0]
	for nm, member := range pkg.Members {
		gen(w, nm, member)
	}
	w.w("\ndata $fmt = { b \"%%d\\n\", b 0 }\n")
	fmt.Fprintf(stdout, "---- QBE%s\n", qbe.Bytes())
	if err := libqbe.Main(libqbe.DefaultTarget(goos, goarch), "fib.qbe", &qbe, &asm, nil); err != nil {
		panic(err)
	}

	fmt.Fprintf(stdout, "---- ASM\n%s\n", asm.Bytes())
	s := filepath.Join(dir, "fib.s")
	if os.WriteFile(s, asm.Bytes(), 0660); err != nil {
		panic(err)
	}

	ccfg, err := cc.NewConfig(goos, goarch)
	if err != nil {
		panic(err)
	}

	bin := filepath.Join(dir, "fib")
	if out, err = exec.Command(ccfg.CC, "-o", bin, s).CombinedOutput(); err != nil {
		panic(fmt.Errorf("\nout=%s FAIL err=%v", out, err))
	}

	if out, err = exec.Command("sh", "-c", fmt.Sprintf("ls -l %s ; file %[1]s ; time %[1]s", bin)).CombinedOutput(); err != nil {
		panic(fmt.Errorf("\nout=%s FAIL err=%v", out, err))
	}

	fmt.Fprintf(stdout, "---- Outputs\n%s", out)
}

type writer struct{ out io.Writer }

func (w *writer) w(s string, args ...interface{}) { fmt.Fprintf(w.out, s, args...) }

func gen(w *writer, nm string, member ssa.Member) {
	if nm == "init" {
		return
	}

	switch x := member.(type) {
	case *ssa.Function:
		function(w, nm, x)
	}
}

func function(w *writer, nm string, f *ssa.Function) {
	w.w("\n")
	if nm == "main" {
		w.w("export ")
	}
	w.w("function ")
	sig := f.Signature
	switch results := sig.Results(); {
	case results != nil:
		w.w("%s ", typ(results.At(0).Type()))
	default:
		w.w("w ")
	}
	w.w("$%s(", nm)
	params := sig.Params()
	for i := 0; i < params.Len(); i++ {
		if i != 0 {
			w.w(", ")
		}
		param := params.At(i)
		w.w("%s %%%s", typ(param.Type()), param.Name())
	}
	w.w(") {")
	for _, b := range f.Blocks {
		block(w, b)
	}
	w.w("\n}\n")
}

func block(w *writer, n *ssa.BasicBlock) {
	w.w("\n@.%v", n.Index)
	for _, instr := range n.Instrs {
		switch x := instr.(type) {
		case *ssa.Call:
			call(w, x)
		case *ssa.Return:
			ret(w, x)
		case *ssa.BinOp:
			binop(w, x)
		case *ssa.If:
			jnz(w, x)
		}
	}
}

func jnz(w *writer, n *ssa.If) {
	w.w("\n\tjnz %s, @.%v, @.%v", op(n.Cond), n.Block().Succs[0].Index, n.Block().Succs[1].Index)
}

func binop(w *writer, n *ssa.BinOp) {
	switch n.Op {
	case token.ADD:
		w.w("\n\t%%%s =%s add %s, %s", n.Name(), typ(n.Type()), op(n.X), op(n.Y))
	case token.SUB:
		w.w("\n\t%%%s =%s sub %s, %s", n.Name(), typ(n.Type()), op(n.X), op(n.Y))
	case token.LSS:
		w.w("\n\t%%%s =w csltl %s, %s", n.Name(), op(n.X), op(n.Y))
	}
}

func ret(w *writer, n *ssa.Return) {
	switch len(n.Results) {
	case 0:
		w.w("\n\tret 0")
	case 1:
		w.w("\n\tret %%%s", n.Results[0].Name())
	}
}

func call(w *writer, n *ssa.Call) {
	w.w("\n\t")
	if _, ok := n.Common().Value.(*ssa.Function); ok {
		w.w("%%%s =%s call $%s(", n.Name(), typ(n.Type()), n.Common().Value.Name())
		for i, v := range n.Common().Args {
			if i != 0 {
				w.w(", ")
			}
			w.w("%s %s", typ(v.Type()), op(v))
		}
		w.w(")")
		return
	}

	w.w("call $printf(l $fmt, ..., l %%%s)", n.Common().Args[0].Name())
}

func op(v ssa.Value) string {
	switch x := v.(type) {
	case *ssa.Const:
		return fmt.Sprintf("%d", x.Int64())
	default:
		return fmt.Sprintf("%%%s", v.Name())
	}
}

func typ(t types.Type) string {
	if t.(*types.Basic).Kind() == types.Bool {
		return "w"
	}
	return "l"
}
